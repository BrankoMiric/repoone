package com.madhatter.controllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.madhatter.DTOs.DepartmentDTO;
import com.madhatter.models.Department;
import com.madhatter.services.IDepartmentsService;

import static com.madhatter.controllers.DepartmentsController.BASE;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = BASE)
public class DepartmentsController {

	protected final static String BASE = "departments/";
	private final static String GET_ALL = "department";
	private final static String GET_BY_NAME = "department/{name}";
	private final static String POST_NEW = "department";
	private final static String DELETE = "department/{id}";
	private final static String GET_CHANGE_NAME = "department/{currentName}/{newName}";

	@Autowired
	private IDepartmentsService departmentsService;
	@Autowired
	private ModelMapper modelMapper;

	public DepartmentsController(IDepartmentsService departmentsService, ModelMapper modelMapper) {
		this.departmentsService = departmentsService;
		this.modelMapper = modelMapper;
	}

	@RequestMapping(value = GET_ALL, method = RequestMethod.GET)
	public List<DepartmentDTO> getAll() {
		List<DepartmentDTO> dtos = new ArrayList<>();
		for (Department department : departmentsService.getAllDepartments()) {
			DepartmentDTO dto = modelMapper.map(department, DepartmentDTO.class);
			dtos.add(dto);
		}
		return dtos;
	}

	@RequestMapping(value = GET_BY_NAME, method = RequestMethod.GET)
	public DepartmentDTO getByName(@PathVariable String name) {
		Department department = departmentsService.getDepartmentByName(name);
		return modelMapper.map(department, DepartmentDTO.class);
	}

	@RequestMapping(value = POST_NEW, method = RequestMethod.POST)
	public DepartmentDTO createNew(@RequestBody DepartmentDTO departmentDTO) {
		Department department = modelMapper.map(departmentDTO, Department.class);
		Department newDepartment = departmentsService.createNewDepartment(department);
		return modelMapper.map(newDepartment, DepartmentDTO.class);
	}

	@RequestMapping(value = DELETE, method = RequestMethod.DELETE)
	public DepartmentDTO deleteDepartment(@PathVariable int id) {
		Department deletedDepartement = departmentsService.deleteDepartment(id);
		return modelMapper.map(deletedDepartement, DepartmentDTO.class);
	}

	@RequestMapping(value = GET_CHANGE_NAME, method = RequestMethod.GET)
	public DepartmentDTO changeName(@PathVariable String currentName, @PathVariable String newName) {
		Department changedDepartment = departmentsService.changeDepartmentName(currentName, newName);
		return modelMapper.map(changedDepartment, DepartmentDTO.class);
	}
}
