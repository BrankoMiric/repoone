package com.madhatter.models;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class Department {
	private int id;
	private String name;
	private int numberOfEmployees;
	private List<Employee> employees;

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public Department() {
	}

	public Department(int id, String name, int numberOfEmployees) {
		this.id = id;
		this.name = name;
		this.numberOfEmployees = numberOfEmployees;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumberOfEmployees() {
		return numberOfEmployees;
	}

	public void setNumberOfEmployees(int numberOfEmployees) {
		this.numberOfEmployees = numberOfEmployees;
	}

	@Override
	public String toString() {
		return "Department [id=" + id + ", name=" + name + ", numberOfEmployees=" + numberOfEmployees + "]";
	}

}
