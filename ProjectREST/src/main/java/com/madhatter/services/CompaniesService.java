package com.madhatter.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.madhatter.models.Company;
import com.madhatter.repositories.ICompaniesRepository;

@Service
public class CompaniesService implements ICompaniesService {

	@Autowired
	private ICompaniesRepository companiesRepository;

	public CompaniesService(ICompaniesRepository companiesRepository) {
		this.companiesRepository = companiesRepository;
	}

	@Override
	public List<Company> getAllCompanies() {
		return companiesRepository.getAllCompanies();
	}

	@Override
	public List<Company> getCompaniesByCity(String city) {
		return companiesRepository.getCompaniesByCity(city);
	}

	@Override
	public Company createNewCompany(Company company) {
		return companiesRepository.createNewCompany(company);
	}

	@Override
	public Company updateCompanyData(int id, Company company) {
		return companiesRepository.updateCompany(id, company);
	}

	@Override
	public Company deleteCompany(int id) {
		return companiesRepository.deleteCompany(id);
	}

}
