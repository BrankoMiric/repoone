package com.madhatter.services;

import java.util.List;

import com.madhatter.models.Company;

public interface ICompaniesService {

	List<Company> getAllCompanies();

	Company createNewCompany(Company company);

	Company updateCompanyData(int id, Company company);

	Company deleteCompany(int id);

	List<Company> getCompaniesByCity(String city);

}