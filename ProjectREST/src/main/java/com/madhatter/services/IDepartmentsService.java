package com.madhatter.services;

import java.util.List;

import com.madhatter.models.Department;
import com.madhatter.repositories.IDepartmentsRepository;

public interface IDepartmentsService {

	void setDepartmentsRepository(IDepartmentsRepository departmentsRepository);

	List<Department> getAllDepartments();

	Department getDepartmentByName(String name);

	Department createNewDepartment(Department department);

	Department deleteDepartment(int id);

	Department changeDepartmentName(String currentName, String newName);

}