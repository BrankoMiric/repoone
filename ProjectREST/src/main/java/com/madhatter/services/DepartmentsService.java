package com.madhatter.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.madhatter.models.Department;
import com.madhatter.repositories.IDepartmentsRepository;

@Service
public class DepartmentsService implements IDepartmentsService {

	@Autowired
	private IDepartmentsRepository departmentsRepository;

	@Override
	public void setDepartmentsRepository(IDepartmentsRepository departmentsRepository) {
		this.departmentsRepository = departmentsRepository;
	}

	@Override
	public List<Department> getAllDepartments() {
		return departmentsRepository.getAllDepartments();
	}

	@Override
	public Department getDepartmentByName(String name) {
		return departmentsRepository.getDepartmentByName(name);
	}

	@Override
	public Department createNewDepartment(Department department) {
		return departmentsRepository.createNewDepartment(department);
	}

	@Override
	public Department deleteDepartment(int id) {
		return departmentsRepository.deleteDepartment(id);
	}

	@Override
	public Department changeDepartmentName(String currentName, String newName) {
		return departmentsRepository.changeDepartmentName(currentName, newName);
	}
}
