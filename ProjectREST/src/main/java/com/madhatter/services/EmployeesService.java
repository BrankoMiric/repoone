package com.madhatter.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.madhatter.models.Employee;
import com.madhatter.repositories.IEmployeesRepository;

@Service
public class EmployeesService implements IEmployeesService {

	@Autowired
	private IEmployeesRepository employeesRepository;

	public EmployeesService(IEmployeesRepository employeesRepository) {
		this.employeesRepository = employeesRepository;
	}

	@Override
	public List<Employee> getAllEmployees() {
		return employeesRepository.getAllEmployees();
	}

	@Override
	public int getTotalNumberOfEmployeesByDepartment(String department) {
		return employeesRepository.getTotalNumberOfEmployeesByDepartment(department);
	}

	@Override
	public Employee createEmploye(Employee employee) {
		return employeesRepository.createEmploye(employee);
	}

	@Override
	public Employee deleteEmploye(int id) {
		return employeesRepository.deleteEmploye(id);
	}

	@Override
	public List<Employee> deleteEmployeesByDepartment(String department) {
		return employeesRepository.deleteEmployeesByDepartment(department);
	}

	@Override
	public Employee changeDepartmentNameForEmployee(int id, String newDepartment) {
		return employeesRepository.changeDepartmentNameForEmployee(id, newDepartment);
	}

	@Override
	public Employee getEmployeeWithHighestSalary() {
		return employeesRepository.getEmployeeWithBiggestSalary();
	}
}
