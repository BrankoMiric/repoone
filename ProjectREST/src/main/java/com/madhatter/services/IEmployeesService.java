package com.madhatter.services;

import java.util.List;

import com.madhatter.models.Employee;

public interface IEmployeesService {

	List<Employee> getAllEmployees();

	int getTotalNumberOfEmployeesByDepartment(String department);

	Employee createEmploye(Employee employee);

	Employee deleteEmploye(int id);

	List<Employee> deleteEmployeesByDepartment(String department);

	Employee changeDepartmentNameForEmployee(int id, String newDepartment);

	Employee getEmployeeWithHighestSalary();

}