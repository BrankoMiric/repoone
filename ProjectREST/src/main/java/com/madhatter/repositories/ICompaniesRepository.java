package com.madhatter.repositories;

import java.util.List;

import com.madhatter.models.Company;

public interface ICompaniesRepository {

	List<Company> getAllCompanies();

	List<Company> getCompaniesByCity(String city);
	
	Company createNewCompany(Company company);

	Company updateCompany(int id, Company company);

	Company deleteCompany(int id);

}