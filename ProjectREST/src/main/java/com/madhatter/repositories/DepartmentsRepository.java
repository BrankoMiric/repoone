package com.madhatter.repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.madhatter.models.Department;

@Repository
public class DepartmentsRepository implements IDepartmentsRepository {

	private List<Department> departments = new ArrayList<>();

	@Autowired
	private IEmployeesRepository employeesRepository;

	public DepartmentsRepository(IEmployeesRepository employeesRepository) {
		this.employeesRepository = employeesRepository;
		Department it = new Department(1, "IT", 100);
		Department hr = new Department(2, "HR", 200);
		Department accounting = new Department(3, "Accounting", 150);
		it.setEmployees(employeesRepository.getAllEmployees().stream().filter(e -> e.getDepartment().equals("IT"))
				.collect(Collectors.toList()));
		hr.setEmployees(employeesRepository.getAllEmployees().stream().filter(e -> e.getDepartment().equals("HR"))
				.collect(Collectors.toList()));
		accounting.setEmployees(employeesRepository.getAllEmployees().stream()
				.filter(e -> e.getDepartment().equals("Accounting")).collect(Collectors.toList()));
		departments.add(it);
		departments.add(hr);
		departments.add(accounting);
	}

	@Override
	public List<Department> getAllDepartments() {
		return departments;
	}

	@Override
	public Department getDepartmentByName(String name) {
		return departments.stream().filter(d -> d.getName().equals(name)).findFirst().orElse(null);
	}

	@Override
	public Department createNewDepartment(Department department) {
		departments.add(department);
		return department;
	}

	@Override
	public Department deleteDepartment(int id) {
		Department department = departments.stream().filter(d -> d.getId() == id).findFirst().get();
		departments.remove(department);
		return department;
	}

	@Override
	public Department changeDepartmentName(String currentName, String newName) {
		Department department = departments.stream().filter(d -> d.getName().equals(currentName)).findFirst()
				.orElse(null);
		if (department != null) {
			department.setName(newName);
		}
		return department;
	}
}
