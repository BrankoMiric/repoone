package com.madhatter.repositories;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.madhatter.models.Employee;

@Repository
public class EmployeesRepository implements IEmployeesRepository {

	private List<Employee> employees = new ArrayList<>();

	public EmployeesRepository() {
		Employee jeff = new Employee(1, "Jeff Bezos", "555-333", "jeff@amazon.com", new BigDecimal(1000000.00), "IT");
		Employee steve = new Employee(2, "Steve Wozniak", "555-333", "steve@apple.com", new BigDecimal(900000.00),
				"IT");
		Employee phj = new Employee(3, "Perla Haney-Jardine", "555-333", "phj@apple.com", new BigDecimal(800000.00),
				"HR");
		Employee richard = new Employee(4, "Richard Pryor", "555-333", "richie@gmail.com", new BigDecimal(70000.00),
				"HR");
		employees.add(jeff);
		employees.add(steve);
		employees.add(phj);
		employees.add(richard);
	}

	@Override
	public List<Employee> getAllEmployees() {
		return employees;
	}

	@Override
	public int getTotalNumberOfEmployeesByDepartment(String department) {
		return (int) employees.stream().filter(e -> e.getDepartment().equals(department)).count();
	}

	@Override
	public Employee createEmploye(Employee employee) {
		employees.add(employee);
		return employee;
	}

	@Override
	public Employee deleteEmploye(int id) {
		Employee employee = employees.stream().filter(e -> e.getId() == id).findFirst().get();
		employees.remove(employee);
		return employee;
	}

	@Override
	public List<Employee> deleteEmployeesByDepartment(String department) {
		List<Employee> employeesByDepartment = employees.stream().filter(e -> e.getDepartment().equals(department))
				.collect(Collectors.toList());
		employees.removeAll(employeesByDepartment);
		return employeesByDepartment;
	}

	@Override
	public Employee changeDepartmentNameForEmployee(int id, String newDepartment) {
		employees.stream().filter(e -> e.getId() == id).findFirst().orElse(null).setDepartment(newDepartment);
		return employees.stream().filter(e -> e.getId() == id).findFirst().orElse(null);
	}

	@Override
	public Employee getEmployeeWithBiggestSalary() {
		return employees.stream().max((e1, e2) -> Integer.compare(e1.getSalary().intValue(), e2.getSalary().intValue()))
				.get();
	}
}
