package com.madhatter.repositories;

import java.util.List;

import com.madhatter.models.Department;

public interface IDepartmentsRepository {

	List<Department> getAllDepartments();

	Department getDepartmentByName(String name);

	Department createNewDepartment(Department department);

	Department deleteDepartment(int id);

	Department changeDepartmentName(String currentName, String newName);

}