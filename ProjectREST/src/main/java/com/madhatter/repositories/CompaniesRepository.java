package com.madhatter.repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.madhatter.models.Company;

@Repository
public class CompaniesRepository implements ICompaniesRepository {

	private List<Company> companies = new ArrayList<>();

	@Autowired
	private IDepartmentsRepository departmentsRepository;

	public CompaniesRepository(IDepartmentsRepository departmentsRepository) {
		this.departmentsRepository = departmentsRepository;
		Company apple = new Company(1, "Apple Inc", "USA", "Cupertino", "Apple Park, 1 Apple Park Way");
		Company amazon = new Company(2, "Amazon", "USA", "Seattle", "South Lake Union");
		apple.setDepartments(departmentsRepository.getAllDepartments());
		amazon.setDepartments(departmentsRepository.getAllDepartments());
		companies.add(apple);
		companies.add(amazon);
	}

	@Override
	public List<Company> getAllCompanies() {
		return companies;
	}

	@Override
	public List<Company> getCompaniesByCity(String city) {
		return companies.stream().filter(c -> c.getCity().equals(city)).collect(Collectors.toList());
	}

	@Override
	public Company createNewCompany(Company company) {
		companies.add(company);
		return company;
	}

	@Override
	public Company updateCompany(int id, Company company) {
		Company prevData = companies.stream().filter(c -> c.getId() == id).findFirst().orElse(null);
		companies.set(companies.indexOf(prevData), company);
		return company;
	}

	@Override
	public Company deleteCompany(int id) {
		Company prevData = companies.stream().filter(c -> c.getId() == id).findFirst().orElse(null);
		companies.remove(prevData);
		return prevData;
	}
}
